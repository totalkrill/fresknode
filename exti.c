#include "ch.h"
#include "dw1000.h"
#include "exti.h"

extern dw1000_driver_t dw;

static THD_WORKING_AREA(mythreadwa,2048);
static thread_reference_t trp = NULL;

const EXTConfig extcfg = {
  {
    {EXT_CH_MODE_DISABLED, NULL},
    {EXT_CH_MODE_DISABLED, NULL},
    {EXT_CH_MODE_DISABLED, NULL},
    {EXT_CH_MODE_DISABLED, NULL},
    {EXT_CH_MODE_RISING_EDGE | EXT_CH_MODE_AUTOSTART | EXT_MODE_GPIOC, extcb1},
    {EXT_CH_MODE_DISABLED, NULL},
    {EXT_CH_MODE_DISABLED, NULL},
    {EXT_CH_MODE_DISABLED, NULL},
    {EXT_CH_MODE_DISABLED, NULL},
    {EXT_CH_MODE_DISABLED, NULL},
    {EXT_CH_MODE_DISABLED, NULL},
    {EXT_CH_MODE_DISABLED, NULL},
    {EXT_CH_MODE_DISABLED, NULL},
    {EXT_CH_MODE_DISABLED, NULL},
    {EXT_CH_MODE_DISABLED, NULL},
    {EXT_CH_MODE_DISABLED, NULL}
  }
};

event_source_t dw1000_event_source;
THD_FUNCTION(myThread, arg) {
  (void)arg;
    chRegSetThreadName("DW1000 interrupt thread");
    trp = chThdGetSelfX();

  while (true) {

    /* Waiting for the IRQ to happen.*/
      //TODO: check correct event
    chEvtWaitAll(1);
    /* Perform processing here.*/
    dw1000_irq_event(&dw);
  }
}

void start_thd(void){

    chThdCreateStatic(mythreadwa, sizeof(mythreadwa),
                          HIGHPRIO, myThread, NULL);
}

void extcb1(EXTDriver *extp, expchannel_t channel) {
  (void)extp;
  (void)channel;
  chSysLockFromISR();
  //chThdResumeI(&trp, (msg_t)0x1337);  /* Resuming the thread.*/
  //dw1000_irq_event(&dw);
  if( trp != NULL)
  {
    chEvtSignalI(trp, 1);
  }
  chSysUnlockFromISR();
}

void start_interrupt_handler(){
    // start thread to be activated by interrupt.
    start_thd();
    extStart(&EXTD1, &extcfg);
}

