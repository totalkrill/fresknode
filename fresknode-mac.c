/**
 * @file fresknode-mac.c
 * @brief
 * @author Kristoffer Ödmark
 * @version 1.0
 * @date 2016-09-13
 */

#include "ch.h"
#include "hal.h"
#include "dw1000.h"
#include "fresknode.h"
#include "fresknode-mac.h"

#include "positioning.h"


#include "chprintf.h"
extern BaseSequentialStream debug_print;
extern SerialUSBDriver SDU1;

#define printf(...) chprintf(&debug_print, __VA_ARGS__)


struct neighbours_t{
    int amount;
    neighbour_t anchors[MAX_NEIGHBOURS];
};

static struct neighbours_t neighbours = { .amount = 0};

void update_neighbour_seen(uint64_t addr, uint64_t now){
        for(int i = 0; i < neighbours.amount; i++){
            if(neighbours.anchors[i].addr.longaddr == addr)
                neighbours.anchors[i].lastseen = now;
        }
}
static int index = 0;
ieee_addr_t next_anchor(void){

                    index++;
                    if(index >= neighbours.amount)
                        index = 0;

                    return neighbours.anchors[index].addr;

}

void check_anchors(void) {
    // basic thread to announce presence continous
        for(int i = 0; i < neighbours.amount; i++){
            volatile uint64_t now = chVTGetSystemTime();
            volatile uint64_t timelimit = MS2ST(3*ANCHOR_BLINKTIME);
            if((now - neighbours.anchors[i].lastseen) > timelimit) {
                // remove the node from the list
                printf("Removing anchor at index %i, with id %8x%8x\n",
                            i,
                            *((uint32_t *)&(neighbours.anchors[i].addr) +1),
                            *((uint32_t *)&(neighbours.anchors[i].addr)));

                for(int j = i; j < (neighbours.amount-1); j++)
                {
                    neighbours.anchors[j].addr = neighbours.anchors[j+1].addr;
                    neighbours.anchors[j].lastseen = neighbours.anchors[j+1].lastseen;
                    neighbours.anchors[j].pos = neighbours.anchors[j+1].pos;
                }
                neighbours.amount--;
                i = 0;
            }
        }
        if(neighbours.amount < 3){
            // 3d positioning is not available
            palSetPad(GPIOC,GPIOC_LED_RED);
        }
        else{
            palClearPad(GPIOC,GPIOC_LED_RED);
        }
}

void mac_frame_recieved(dw1000_driver_t *self){
    pingmsg_t *msg = (pingmsg_t *)(self->rx_parsed.payload);
    ieee_addr_t source;
    source = self->rx_parsed.source;
    printf("BEACON:%8x%8x,%i,%i,%i,%f\n", *((uint32_t *)&source + 1),
           *((uint32_t *)&source),
           msg->pos.x,
           msg->pos.y,
           msg->pos.z,
           self->rx_frame[0].rssi
           );

    if(msg->device == DEVICE_ANCHOR || msg->device == DEVICE_SINK){
        bool new_node = true;
        for(int i = 0; i < neighbours.amount; i++){
            if(neighbours.anchors[i].addr.longaddr == source.longaddr){
                // update node and exit loop
                new_node = false;
                //neighbours.anchors[i].lastseen = msg->network_time;
                neighbours.anchors[i].lastseen = chVTGetSystemTimeX();
                neighbours.anchors[i].pos = msg->pos;
                break;
            }
        }
        // add the new node, either overwriting the oldest entry, or adding
        if(new_node){
           if(neighbours.amount >= MAX_NEIGHBOURS){
                //overwrite oldest
               uint64_t oldest = 0xFFFFFFFFFFFFFFFF;
               int index;
               for(int i = 0; i < MAX_NEIGHBOURS; i++){
                    uint64_t now = neighbours.anchors[i].lastseen;
                    if(now < oldest){
                        oldest = now;
                        index = i;
                    }
               }
               neighbours.anchors[index].pos = msg->pos ;
               neighbours.anchors[index].addr = source;
                //neighbours.anchors[index].lastseen = msg->network_time;
               neighbours.anchors[index].lastseen = chVTGetSystemTimeX();

           }
           else{
               //add to list
               neighbours.anchors[neighbours.amount].pos = msg->pos ;
               neighbours.anchors[neighbours.amount].addr = source;
                //neighbours.anchors[neighbours.amount].lastseen = msg->network_time;
               neighbours.anchors[neighbours.amount].lastseen = chVTGetSystemTimeX();
               neighbours.amount++;
           }
        }

    }
}

extern dw1000_driver_t dw;

static THD_WORKING_AREA(tag_thread_wa, 512);
static THD_FUNCTION(tag_thread, u) {
    (void)u;
    chRegSetThreadName("tag_thread");

    while (1) {
        // while(!dw1000_channel_clear_assesment(&dw,3))
        //   chThdSleepMilliseconds(msg_time);
        check_anchors();
        ieee_addr_t addr = next_anchor();
        if (addr.longaddr > 0) {
            // twowayranging_request(&dw, addr, 8);
            singlesided_request(&dw, addr, 8);
        }
        chThdSleepMilliseconds(10);

    }
}

static THD_WORKING_AREA(blink_thread_wa, 512);
static THD_FUNCTION(blink_thread, u) {
    // basic thread to announce presence continous
    (void)u;
    chRegSetThreadName("blink_thread");
    static ieee802_15_4_frame_parameters_t p;
    p.sequence = 0;
    p.type = IEEE_FRAME_TYPE_MAC;
    p.security = false;
    p.ranging = false;
    p.request_ack = false;
    p.frame_pending = false;

    bytecpy((uint8_t *)&(p.source), (const uint8_t*)&((&dw)->config->euid.u64), 8);
    p.source_len = 8;
    p.source_panid = (&dw)->config->panid;
    uint16_t destination = 0xFFFF;

    bytecpy((uint8_t *)(&(p.destination)), (const uint8_t *)(&destination), 2);
    p.destination_len = 2;
    p.destination_panid = p.source_panid;

    while (true) {
        pingmsg_t payload;

        GetNodePosition(&(payload.pos));
        payload.device = DEVICE_ANCHOR;
        payload.network_time = chVTGetSystemTimeX();

        finished_frame_t result_frame;
        result_frame.payload = ((&dw)->txbuf);
        p.payload_length = sizeof(pingmsg_t);;
        p.payload = (uint8_t *)&payload;
        create_802_15_4_frame(&p, &result_frame);
        dw1000_send(&dw, false, result_frame.payload, result_frame.length);
        int random = random_int();
        int r = random%100;
        chThdSleepMilliseconds(ANCHOR_BLINKTIME+r);
        palTogglePad(GPIOC, GPIOC_LED_GREEN);
    }
}

void start_blinkerthread(void){
        chThdCreateStatic(blink_thread_wa, sizeof(blink_thread_wa),
                NORMALPRIO, blink_thread, NULL);
}

void start_tagthread(void){
        chThdCreateStatic(tag_thread_wa, sizeof(tag_thread_wa),
                NORMALPRIO, tag_thread, NULL);
}
