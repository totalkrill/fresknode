# Modules directory location from Makefile
MODULE_DIR = ./modules

# Imported source files and paths from modules
include $(MODULE_DIR)/usb/usb.mk
include $(MODULE_DIR)/cmd_handler/cmd_handler.mk
include $(MODULE_DIR)/timer/timer.mk
include $(MODULE_DIR)/uart/uart.mk
include $(MODULE_DIR)/eepromstorage/eeprom_storage.mk
include $(MODULE_DIR)/cli/cli_parser.mk
#include $(MODULE_DIR)/ringbuffer/ringbuffer.mk

include $(MODULE_DIR)/version_information/version_information.mk

# List of all the module related files.
#MODULES_SRC = 	$(USB_SRCS) \
#			  	$(TIMER_SRCS) \
#			  	$(UARTSRC) \
#              	$(VERSIONINFO_SRCS) \
#				$(MODULE_DIR)/debug_print/debug_print.c \
#				$(CMD_SRCS) \
#				$(RINGBUFFER_SRCS)
#
MODULES_SRC :=$(shell find $(MODULE_DIR) -name '*.c')

# Required include directories
MODULES_INC = 	$(USB_INC) \
			  	$(TIMER_INC) \
			  	$(UARTINC) \
              	$(VERSIONINFO_INC) \
				$(MODULE_DIR)/debug_print \
				$(CMD_INC) \
				$(RINGBUFFER_INC) \
				$(MODULE_DIR)/cobs \
				$(EEPROMSTORAGE_INC) \
				$(CLI_INC)



