/**
 * @file timer.h
 * @brief  Header for timer functions
 * @author Kristoffer Ödmark
 * @version 0.1
 * @date 2015-08-27
 */
#ifndef TIMER_H_
#define TIMER_H_
#include "ch.h"
#include "hal.h"
#include "stdint.h"
#include "string.h"

#define TIMER_MAXLEN 100
#define TIMER_MARKER_MAXLEN 10

#define S(x) #x
#define S_(x) S(x)
#define S__LINE__ S_(__LINE__)

// now S__LINE__ will be the line number as a string

typedef struct
{
    time_measurement_t timer;
    uint32_t accumulated_time;
    uint32_t timevector[TIMER_MAXLEN];
    const char *marks[TIMER_MAXLEN];
    uint8_t counter;
    bool running;
    const char name[];
} my_timer_t;

/**
 * @brief  Inititate the timer module.
 *
 * @param timer
 */
void init_timer(my_timer_t *timer);

/**
 * @brief  Start timer.
 *
 * @param timer
 */
void start_timer(my_timer_t *timer);

/**
 * @brief  Get time from when timer was started.
 * This will not stop the timer. Just get a time in us from when it stared.
 *
 * @param timer
 *
 * @returns time from start in us.
 */
uint32_t set_timer_mark(my_timer_t *timer, const char mark[]);

/**
 * @brief Stops the timer and returns the time.
 *
 * @param timer
 *
 * @returns time from start in us.
 */
uint32_t stop_timer(my_timer_t *timer);

/**
 * @brief  Print out the timer structure
 *
 * @param timer
 */
void print_timer(my_timer_t *timer);

/* static version of functions. */
/**
 * @brief  Initalize a timex structure;
 */
void init_timex(void);
/**
 * @brief  Start the timer
 */
void start_timex(void);
/**
 * @brief  Do a timer mark, this will store the timer value at a certain point.
 * with a string 'marker' so that it will be easily recognizable.
 *
 * @param mark[TIMER_MARKER_MAXLEN]
 *
 * @returns
 */
uint32_t set_timex_mark(const char mark[]);
/**
 * @brief  Stop the timer,
 *
 * @returns  the accumulated time since the timer was started.
 */
uint32_t stop_timex(void);
/**
 * @brief  print out all the values in the timex structure
 */
void print_timex(void);

#endif
