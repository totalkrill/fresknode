/**
 * @file timer.c
 *
 * @brief  Timer module
 * @author Kristoffer Ödmark
 * @version 0.1
 * @date 2015-08-27
 */
#include    "timer.h"

#undef DEBUG
#define DEBUG 1

#undef printf
#if DEBUG
// include necesary files for debugging output
#include "hal.h"
#include "chprintf.h"
#include "debug_print.h"
extern BaseSequentialStream debug_print;
#define printf(...) chprintf(&debug_print, __VA_ARGS__)

#else

#endif
// create a timer by default to use
static my_timer_t timex = { .name = "timex"};

inline void init_timex(){
    init_timer(&timex);
}

inline void start_timex(){
    start_timer(&timex);
}

inline uint32_t set_timex_mark(const char mark[]){
    return set_timer_mark(&timex, mark);
}

inline uint32_t stop_timex(){
    return stop_timer(&timex);
}
inline void print_timex(){
    print_timer(&timex);
}

void print_timer(my_timer_t *timer){
    uint32_t i = 0;

    printf("Timer: %s\n\r", timer->name);
    printf("nr    : total time : difference : marker\n \r");
    printf("================================================\n\r");
    for(i = 0; i < timer->counter; i++){
        if(i == 0){
            printf("%5u : %10u : %10u : %s\n\r", i, timer->timevector[i],
                    timer->timevector[i], timer->marks[i]);
        }
        else{
            printf("%5u : %10u : %10u : %s\n\r", i, timer->timevector[i],
                    timer->timevector[i] - timer->timevector[i-1], timer->marks[i]);
        }
    }
    printf("================================================\n\r");

}

void init_timer(my_timer_t *timer){

    chTMObjectInit(&timer->timer);
    /* clear timevector */
    uint32_t i;
    for(i = 0; i < sizeof(timer->timevector)/sizeof(uint32_t); i++){
        timer->timevector[i] = 0;
    }

    timer->accumulated_time = 0;
    timer->counter = 0;
}

void start_timer(my_timer_t *timer){
    chTMStartMeasurementX(&timer->timer);
    timer->accumulated_time = 0;
    timer->timevector[0] = 0;
    timer->counter =1;
    timer->running = true;
    timer->marks[0] = "start";
}

uint32_t set_timer_mark(my_timer_t *timer,const char mark[]){
    if(timer->running == false)
        return 0;

    chTMStopMeasurementX(&timer->timer);
    /* Some macro to extract the time in us*/
    uint32_t dt = RTC2US(STM32_SYSCLK, timer->timer.last);
    /* append time to the accumulated value */
    chTMStartMeasurementX(&timer->timer);

    timer->marks[timer->counter] = mark;
    timer->accumulated_time += dt;
    timer->timevector[timer->counter] = timer->accumulated_time;
    timer->counter++;
    if(timer->counter >= sizeof(timer->timevector)/sizeof(uint32_t)){
        timer->counter = 0;
    }

    return timer->accumulated_time;
}

uint32_t stop_timer(my_timer_t *timer){
    chTMStopMeasurementX(&timer->timer);
    /* Some macro to extract the time */
    uint32_t dt = RTC2US(STM32_SYSCLK, timer->timer.last);
    timer->accumulated_time += dt;
    timer->timevector[timer->counter] = timer->accumulated_time;
    timer->marks[timer->counter] = "stop";
    timer->counter++;
    timer->running = false;
    uint32_t returnval = timer->accumulated_time;
    //timer->accumulated_time = 0;
    return returnval;
}
