/**
 * @file eeprom-storage.c
 * @brief  Storage and retrieval functions for the eeprom
 * @author Kristoffer Ödmark
 * @version 1.0
 * @date 2016-10-20
 */

#include "eeprom_storage.h"
#include "dw1000.h"
#include "eeprom.h"
#include "positioning.h"

extern dw1000_hal_t default_dw1000_hal;
static struct __attribute__((packed))
{
    uint16_t antenna_delay_val;
    position_t position;
    uint8_t type; // determines what the node will act like

} setting_storage;

void save_settings(void)
{
    eeprom_write(EEPROM_STORE_ADDRESS, (uint8_t *)(&setting_storage),
                 sizeof(setting_storage));
}

void update_settings(void)
{
    /* setting_storage.antenna_delay_val = */
    /*     dw1000_get_antenna_delay(&default_dw1000_hal); */

    GetNodePosition(&(setting_storage.position));
    setting_storage.type;
}
void restore_settings(void)
{
    eeprom_read(EEPROM_STORE_ADDRESS, (uint8_t *)(&setting_storage),
                sizeof(setting_storage));

    SetNodePosition(&(setting_storage.position));

}
void settings_set_nodetype(nodetype_t type)
{
    setting_storage.type = type;
}

uint8_t settings_get_nodetype()
{
    return setting_storage.type;
}
uint16_t settings_get_antenna_delay()
{
    return setting_storage.antenna_delay_val;
}
