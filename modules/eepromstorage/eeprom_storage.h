/**
 * @file eeprom-storage.h
 * @brief  eeprom storage header
 * @author Kristoffer Ödmark
 * @version 1.0
 * @date 2016-10-20
 */
#ifndef EEPROM_STORAGE_H
#define EEPROM_STORAGE_H

#define EEPROM_STORE_ADDRESS 0x00
#include "stdint.h"

#include "fresknode-mac.h"


typedef enum {
    NODE_UNDEFINED = 0,
    NODE_PASSIVE,
    NODE_MASTER,
    NODE_ANCHOR,
    NODE_TAG,
    NODE_DATA_SENDER,
} nodetype_t;

void save_settings(void);

void restore_settings(void);

void update_settings(void);

// Settings getter/settet;
uint8_t settings_get_nodetype(void);
void settings_set_nodetype(nodetype_t type);
uint16_t settings_get_antenna_delay(void);



#endif /* EEPROM-STORAGE_H */
