/**
 * @file cmd_handler.h
 * @brief
 * @author Kristoffer Ödmark
 * @version 0.1
 * @date 2016-05-10
 */
#ifndef CMD_HANDLER_H
#define CMD_HANDLER_H

#include "dw1000.h"

#undef printf

typedef enum {
    CMD_ACK,
    CMD_LEDS,
    CMD_BLINK,
    CMD_RANGE,
    CMD_RANGE_MULTIPLE,
    CMD_ANTENNA_DELAY,
    CMD_SET_POSITION,
    CMD_SET_TXPWR
} commands_t;

typedef bool (*cmd_function)(dw1000_driver_t *dw, const uint8_t *payload,
                             const uint16_t length);

typedef struct __attribute__((packed))
{
    const commands_t id;
    uint8_t acked_id;
} ack_cmd;

typedef struct __attribute__((packed))
{
    const commands_t id;
    uint8_t leds;
} led_cmd;
typedef struct __attribute__((packed))
{
    const commands_t id;
} blink_cmd;

typedef struct __attribute__((packed))
{
    const commands_t id;
    ieee_addr_t addr;
    uint8_t addr_len;
    uint16_t count;
    uint16_t timeperiod;
} range_cmd;

typedef struct __attribute__((packed))
{
    const commands_t id;
    uint32_t power;

} set_txpwr_cmd;

typedef struct __attribute__((packed))
{
    const commands_t id;
    int32_t x;
    int32_t y;
    int32_t z;

} set_position_cmd;

typedef struct __attribute__((packed))
{
    const commands_t id;
    uint8_t num_addresses;
    uint16_t shortaddr[8];
    uint16_t count;
    uint16_t timeperiod;
} range_multiple_cmd;

typedef struct __attribute__((packed))
{
    const commands_t id;
    uint16_t new_antenna_delay;
} change_antenna_delay_cmd;

typedef struct
{
    uint8_t id;
    uint8_t arg[100];
    uint16_t length;
} command_payload;

void dw1000_command_queue(dw1000_driver_t *dw);
bool dw1000_command_handler(dw1000_driver_t *dw, const uint8_t *payload,
                            uint16_t length);
void vInitCmdHandler(void);

bool dw1000_send_command(dw1000_driver_t *dw, ieee_addr_t addr,
                         uint8_t addr_len, uint8_t *args, uint16_t length);

#endif /* CMD_HANDLER_H */
