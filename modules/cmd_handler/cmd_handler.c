/**
 * @file cmd_handler.c
 * @brief  Command handling structure functions, received data packages should
 * be sent here.
 * @author Kristoffer Ödmark
 * @version 0.1
 * @date 2016-05-10
 */
#include "cmd_handler.h"
#include <string.h>
#include "ch.h"
#include "chprintf.h"
#include "debug_print.h"
#include "dw1000.h"
#include "hal.h"
#include "ieee_types.h"

#include "positioning.h"
#include "eeprom_storage.h"

#undef printf

// include necesary files for debugging output
#include "chprintf.h"
#include "debug_print.h"
#include "hal.h"
extern BaseSequentialStream debug_print;
extern SerialUSBDriver SDU1;
#define printf(...) chprintf(&debug_print, __VA_ARGS__)

bool acknowledge_cmd(dw1000_driver_t *dw, const uint8_t *payload,
                 const uint16_t length)
{
    // always return false, otherwise the ack command will be acked = infinite
    // cycle
    if (length == sizeof(ack_cmd))
    {
        chprintf(&debug_print,"ACK:%u\n", ((ack_cmd *)payload)->acked_id);
    }
    (void)(dw);
    (void)(payload);
    (void)(length);
    return false;
}

bool range_addr(dw1000_driver_t *dw, const uint8_t *payload,
                const uint16_t length)
{
    (void)dw;
    // also remember to remove the fcr from the payload length
    if (length == sizeof(range_cmd))
    {
        range_cmd *cmd = (range_cmd *)payload;
        for (int i = 0; i < cmd->count; i++)
        {
            int32_t r = twowayranging_request(dw, cmd->addr, cmd->addr_len);
            (void)r;
            printf("R %i: %i\n\r", i, r);
            chThdSleepMilliseconds(cmd->timeperiod);
        }
        return true;
    }
    else
    {
        printf("payload length = %i\n\r", length);
        return false;
    }
}

bool range_multiple_shortaddr(dw1000_driver_t *dw, const uint8_t *payload,
                              const uint16_t length)
{
    (void)dw;
    // remove crc from the payload
    if (length == sizeof(range_multiple_cmd))
    {
        volatile range_multiple_cmd *cmd = (range_multiple_cmd *)payload;
        if (cmd->num_addresses > 0 || cmd->num_addresses < 8)
        {
            for (int i = 0; i < cmd->count; i++)
            {
                for (int j = 0; j < cmd->num_addresses; j++)
                {
                    int32_t r = twowayranging_request_wait(
                        dw, (ieee_addr_t)cmd->shortaddr[j], 2);
                    (void)r;
                    printf("R %i,%i: %i\n\r", cmd->shortaddr[j], i, r);
                    chThdSleepMilliseconds(cmd->timeperiod);
                }
            }
            return true;
        }
        else
            return false;
    }
    else
    {
        printf("payload length = %i\n\r", length);
        return false;
    }
}

bool toggle_led(dw1000_driver_t *dw, const uint8_t *payload,
                const uint16_t length)
{
    (void)dw;
    // also remember to remove the fcr from the payload length
    if (length == sizeof(led_cmd))
    {
        led_cmd *cmd = (led_cmd *)payload;
        printf("leds %x\n\r", cmd->leds);
        if (cmd->leds & 0x1) palTogglePad(GPIOC, GPIOC_LED_GREEN);
        if (cmd->leds & 0x2) palTogglePad(GPIOC, GPIOC_LED_BLUE);
        if (cmd->leds & 0x4) palTogglePad(GPIOC, GPIOC_LED_RED);
        return true;
    }
    else
    {
        printf("payload length = %i\n\r", length);
        return false;
    }
}

bool blink_leds(dw1000_driver_t *dw, const uint8_t *payload,
                const uint16_t length)
{
    (void)dw;
    (void)payload;
    if (length == sizeof(blink_cmd))
    {
        //blink_cmd *cmd = (blink_cmd *)payload;
        printf("blinking\n");
        for (int i = 0; i < 50; i++)
        {
            palTogglePad(GPIOC, GPIOC_LED_GREEN);
            palTogglePad(GPIOC, GPIOC_LED_BLUE);
            palTogglePad(GPIOC, GPIOC_LED_RED);
            chThdSleepMilliseconds(200);
        }
        return true;
    }
    else
    {
        printf("payload length = %i\n\r", length);
        return false;
    }
}
bool change_antenna_delay(dw1000_driver_t *dw, const uint8_t *payload,
                          const uint16_t length)
{
    (void)dw;
    // also remember to remove the fcr from the payload length
    if (length == sizeof(change_antenna_delay_cmd))
    {
        change_antenna_delay_cmd *cmd = (change_antenna_delay_cmd *)payload;
        chprintf(&debug_print, "new antenna delay =  %u\n\r",
                 cmd->new_antenna_delay);
        dw1000_set_antenna_delay(dw->hal, cmd->new_antenna_delay);
        return true;
    }
    else
    {
        printf("payload length = %i\n\r", length);
        return false;
    }
}
bool set_position(dw1000_driver_t *dw, const uint8_t *payload,
                  const uint16_t length)
{
    (void)dw;

    if (length == sizeof(set_position_cmd))
    {
        set_position_cmd *cmd = (set_position_cmd *)payload;
        chprintf(&debug_print, "new position: x:%i, y:%i, z:%i\n", cmd->x,
                 cmd->y, cmd->z);
        position_t pos = {.x = cmd->x, .y = cmd->y, .z = cmd->z};

        SetNodePosition(&pos);
        update_settings();
        save_settings();

        return true;
    }
    else
    {
        printf("payload length mismatch = %i\n\r", length);
        return false;
    }
}

bool set_txpwr(dw1000_driver_t *dw, const uint8_t *payload,
         const uint16_t length)
{
    if(length == sizeof(set_txpwr_cmd))
    {
        uint32_t txpwr = ((set_txpwr_cmd *)payload)->power;
        dw1000_set_tx_power(dw->hal, DW1000_MANUAL_POWER, txpwr);
        return true;
    }
    return false;
}

const cmd_function command_list[] = {
    acknowledge_cmd,
    toggle_led,
    blink_leds,
    range_addr,
    range_multiple_shortaddr,
    change_antenna_delay,
    set_position,
    set_txpwr,
};

bool dw1000_send_command(dw1000_driver_t *dw, ieee_addr_t addr,
                         uint8_t addr_len, uint8_t *args, uint16_t length)
{
    ieee802_15_4_frame_parameters_t p;
    p.sequence = 0;
    p.type = IEEE_FRAME_TYPE_DATA;
    p.security = false;
    p.ranging = false;
    p.request_ack = false;
    p.frame_pending = false;
    bytecpy((uint8_t *)&(p.source), (const uint8_t *)&(dw->config->euid.u64),
            8);
    p.source_len = 8;
    p.source_panid = dw->config->panid;

    bytecpy((uint8_t *)(&(p.destination)), (const uint8_t *)(&addr), addr_len);
    p.destination_len = addr_len;
    p.destination_panid = p.source_panid;
    finished_frame_t result_frame;
    result_frame.payload = (dw->txbuf);
    p.payload_length = length;
    p.payload = args;
    create_802_15_4_frame(&p, &result_frame);

    dw1000_send(dw, result_frame.ranging, result_frame.payload,
                result_frame.length);
    return true;
}
static thread_reference_t command_trp = NULL;
static THD_WORKING_AREA(waCmdTask, 256);

static dw1000_driver_t *dw_ptr;
static uint16_t cmd_length = 0;
// array that can contain an entire payload
static uint8_t cmd_array[127];

void dw1000_command_queue(dw1000_driver_t *dw)
{
    uint8_t *payload = dw->rx_parsed.payload;
    uint8_t length = dw->rx_parsed.payload_length;
    if (length <= 0) return;
    bytecpy((uint8_t *)&cmd_array, (const uint8_t *)payload, length);
    cmd_length = length;
    dw_ptr = dw;
    if (command_trp != NULL)
    {
        chEvtSignal(command_trp, 2);
    }
}

bool dw1000_command_handler(dw1000_driver_t *dw, const uint8_t *payload,
                            uint16_t length)
{
    chThdSleepMilliseconds(1);

    int command_array_size = sizeof(command_list) / sizeof(cmd_function);
    // the command id will be the offset in the command_list
    command_payload *cmd_args = (command_payload *)payload;
    uint8_t id = cmd_args->id;
    ieee_addr_t source_addr = dw->rx_parsed.source;
    uint8_t source_len = dw->rx_parsed.source_len;

    //printf("command: %x\n\r", cmd_args->id);

    if (id >= command_array_size)
    {
        printf("Command id to large\n\r");
        return false;
    }
    else
    {
        if (command_list[cmd_args->id](dw, payload, length))
            {
                // send an ack command
                ack_cmd ack = {.id = CMD_ACK, .acked_id = id};
                dw1000_send_command(dw, source_addr,
                                    source_len, (uint8_t *)&ack,
                                    sizeof(ack_cmd));
            }
    }

    return false;
}

event_source_t cmd_event_source;

static THD_FUNCTION(CmdHandler, arg)
{
    (void)arg;
    chRegSetThreadName("Command handling thread");
    command_trp = chThdGetSelfX();
    while (true)
    {
        chEvtWaitAll(2);
        dw1000_command_handler(dw_ptr, (uint8_t *)&cmd_array, cmd_length);
    }
}

void vInitCmdHandler(void)
{
    chThdCreateStatic(waCmdTask, sizeof(waCmdTask), NORMALPRIO, CmdHandler,
                      NULL);
}
