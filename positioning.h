/**
 * @file positioning.h
 * @brief  data
 * @author Kristoffer Ödmark
 * @version 1.0
 * @date 2016-10-20
 */
#ifndef POSITIONING_H
#define POSITIONING_H
/* ****************** Position stuff ********************** */
#include "stdint.h"

typedef struct __attribute__((packed))
{
    int32_t x;
    int32_t y;
    int32_t z;
} position_t;

void SetNodePosition(position_t *pos);
void GetNodePosition(position_t *pos);
#endif /* POSITIONING_H */
