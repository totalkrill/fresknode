/**
 * @file main.c
 * @brief program entry point
 * @author Kristoffer Ödmark
 * @version 0.9
 * @date 2015-11-02
 */

#include "ch.h"
#include "hal.h"
#include "usb_related.h"

#include "dw1000.h"

#include "eeprom.h"
#include "exti.h"
#include "ieee_types.h"
#include "my_uart.h"
#include "timer.h"

#include "chprintf.h"
#include "cmd_handler.h"
#include "debug_print.h"

#include "cobs.h"

#include "fresknode-mac.h"
#include "fresknode.h"
#include "positioning.h"

#include "eeprom_storage.h"

#include "stdint.h"

extern BaseSequentialStream debug_print;
extern SerialUSBDriver SDU1;

#define printf(...) chprintf(&debug_print, __VA_ARGS__)

/**
 * @brief Placeholder for error messages.
 */
volatile assert_errors kfly_assert_errors;

extern dw1000_hal_t default_dw1000_hal;

dw1000_driver_t dw;

ieee_euid_t devid;



nodetype_t node_type;

void get_euid(void)
{
    eeprom_init();
    uint8_t uid[8] = {0, 0, 0, 0, 0, 0, 0, 0};
    eeprom_read(EEPROM_UID_ADDRESS, (uint8_t *)uid, 8);
    devid.u8[0] = uid[0];
    devid.u8[1] = uid[1];
    devid.u8[2] = uid[2];
    devid.u8[3] = uid[3];
    devid.u8[4] = uid[4];
    devid.u8[5] = uid[5];
    devid.u8[6] = uid[6];
    devid.u8[7] = uid[7];

    devid.u8[7] = devid.u8[5];
    devid.u8[6] = devid.u8[4];
    devid.u8[5] = devid.u8[3];
    devid.u8[4] = 0xFF;
    devid.u8[3] = 0xFE;
}

void HardFault_Handler(unsigned long *hardfault_args);

void ledStartupSequence() {
    palClearPad(GPIOC, GPIOC_LED_GREEN);
    palClearPad(GPIOC, GPIOC_LED_BLUE);
    palClearPad(GPIOC, GPIOC_LED_RED);
    chThdSleepMilliseconds(500);

    palSetPad(GPIOC, GPIOC_LED_GREEN);
    chThdSleepMilliseconds(500);

    palSetPad(GPIOC, GPIOC_LED_BLUE);
    chThdSleepMilliseconds(500);

    palSetPad(GPIOC, GPIOC_LED_RED);
    chThdSleepMilliseconds(500);

    palClearPad(GPIOC, GPIOC_LED_GREEN);
    palClearPad(GPIOC, GPIOC_LED_BLUE);
    palClearPad(GPIOC, GPIOC_LED_RED);

}
int main(void)
{
    /*
     * System initializations.
     * - HAL initialization, this also initializes the configured
     *   device drivers and performs the board-specific initializations.
     * - Kernel initialization, the main() function becomes a thread
     *   and the RTOS is active.
     */
    halInit();
    chSysInit();
    // SysTick_Config(SystemCoreClock/1000);

    // enable usb
    usbstartup(&dw);
    get_euid();

    restore_settings();

    // rng
    rccEnableAHB2(RCC_AHB2ENR_RNGEN, 0);
    RNG->CR |= RNG_CR_IE;
    RNG->CR |= RNG_CR_RNGEN;

    // enable debug print thread
    /* vInitDebugPrint((BaseSequentialStream *)&SD3); */
    vInitDebugPrint((BaseSequentialStream *)&SDU1);
    sdStart(&SD3, NULL);
    vInitCmdHandler();

    ledStartupSequence();

    printf("dw1000 git descriptor: %s\n\r", DW_GIT);

    palClearPad(GPIOC, GPIOC_DW_RST);
    chThdSleepMilliseconds(200);
    palSetPad(GPIOC, GPIOC_DW_RST);

    printf("EUID: 0x%4x%4x\n\r", devid.u32[1], devid.u32[0]);
    chThdSleepMilliseconds(200);

    // enable interrupt handler
    start_interrupt_handler();

    ieee_shortaddr_t ieee_shortaddr;
    ieee_shortaddr.u16 = 5;

    printf("shortaddr: %i\n\r", ieee_shortaddr.u16);

    dw.state = DW1000_STATE_UNINITIALIZED;

    dw1000_conf_t config;
    config.shortaddr = ieee_shortaddr;

    dw1000_generate_recommended_conf(DW1000_DATARATE_6800, DW1000_CHANNEL_4,
                                     devid, &config);

    dw.config = &config;

    dw.hal = &default_dw1000_hal;
    dw.ranging_module = &singlesided_module;
    // dw.ranging_module = &twowayranging_module;
    dw.packet_module = &peertopeer_module;
    printf("Initializing dw1000 radio\n");
    chThdSleepMilliseconds(200);
    dw1000_init(&dw);
    dw1000_print_config(&dw);

    /* set_twowayranging_callback(&fresknode_minimal_passive_range_callback); */

    set_twowayranging_callback(&fresknode_range_callback);
    set_singlesided_callback(&fresknode_range_callback);
    set_peertopeer_callback(dw1000_command_queue);
    set_peertopeer_mac_callback(mac_frame_recieved);


    dw1000_receive(&dw, 0, 0);

    printf("dw1000 radio receiving\n");
    int per_loop = 1;
    switch( settings_get_nodetype() )
    {
        case NODE_ANCHOR:
            // start presence announcing thread
            printf("Becoming anchor\n\r");
            start_blinkerthread();
            break;
        case NODE_TAG:
            printf("Becoming tag\n\r");
            start_tagthread();
            break;
        case NODE_PASSIVE:
            printf("Becoming passive\n\r");
            set_twowayranging_callback(&fresknode_minimal_passive_range_callback);
            set_singlesided_callback(&fresknode_minimal_passive_range_callback);
            break;
        default:
            printf(
                "Unconfigured node, configure using type <type>\nWhere type "
                "can be: tag, passive, anchor\n");
            break;
    };


    while (1) {


        if (per_loop % 5000 == 0) {
            dw1000_trx_off(&default_dw1000_hal);
            dw1000_softreset_rx(&default_dw1000_hal);
            dw1000_receive(&dw, 0, 0);
            per_loop = 0;
        }
        chThdSleepMilliseconds(10);  // 4msg + 1 msg sleeptime

        per_loop++;
    }
}
