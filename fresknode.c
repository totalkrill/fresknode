/**
 * @file fresknode.c
 * @brief  Fucntions related to the fresknode functionality
 * @author Kristoffer Ödmark
 * @version 0.1
 * @date 2016-06-30
 */
#include "fresknode.h"
#include "fresknode-mac.h"

#include "ch.h"
#include "hal.h"

#include "chprintf.h"
#include "cmd_handler.h"
#include "debug_print.h"
#include "dw1000.h"

#define TRUE_RANGE 5431

#define AVERAGE 1000
static int values = 0;
static int tot_range = 0;

void print_addresses(dw1000_driver_t *dw)
{
    if (dw->rx_parsed.source_len == 8 && dw->rx_parsed.destination_len == 8)
    {
        uint32_t *s1 = (uint32_t *)&(dw->rx_parsed.source);
        uint32_t *s2 = s1 + 1;
        uint32_t *d1 = (uint32_t *)&(dw->rx_parsed.destination);
        uint32_t *d2 = d1 + 1;
        chprintf(&debug_print, "%x%x,%x%x,", *s2, *s1, *d2, *d1);
    }
    if (dw->rx_parsed.source_len == 2 && dw->rx_parsed.destination_len == 8)
    {
        uint16_t *s1 = (uint16_t *)&(dw->rx_parsed.source);
        uint32_t *d1 = (uint32_t *)&(dw->rx_parsed.destination);
        uint32_t *d2 = d1 + 1;
        chprintf(&debug_print, "%x,%x%x,", *s1, *d2, *d1);
    }
    if (dw->rx_parsed.source_len == 8 && dw->rx_parsed.destination_len == 2)
    {
        uint32_t *s1 = (uint32_t *)&(dw->rx_parsed.source);
        uint32_t *s2 = s1 + 1;
        uint16_t *d1 = (uint16_t *)&(dw->rx_parsed.destination);
        chprintf(&debug_print, "%x%x,%x,", *s2, *s1, *d1);
    }
    if (dw->rx_parsed.source_len == 2 && dw->rx_parsed.destination_len == 2)
    {
        uint16_t *s1 = (uint16_t *)&(dw->rx_parsed.source);
        uint16_t *d1 = (uint16_t *)&(dw->rx_parsed.destination);
        chprintf(&debug_print, "%x,%x,", *s1, *d1);
    }
}
void dw1000_range_calibration_callback(dw1000_driver_t *dw,
                                       ranging_result_payload_t *resultmsg)
{
    uint32_t range_tof = resultmsg->tof;
    fresknode_minimal_passive_range_callback(dw, resultmsg);
    values++;
    tot_range += range_tof;

    if (values >= AVERAGE)
    {
        uint32_t med_range = tot_range / AVERAGE;
        tot_range = 0;
        values = 0;

        int32_t error = med_range - (int32_t)mm_to_tof(TRUE_RANGE);
        uint16_t rx_dly = dw1000_get_antenna_delay(dw->hal);

        uint16_t new_rx_dly = rx_dly + (int16_t)error;
        dw1000_set_antenna_delay(dw->hal, new_rx_dly);
        chprintf(&debug_print, "antenna delay was %i, became %i, sending...",
                 rx_dly, new_rx_dly);
        ieee_addr_t addr;
        change_antenna_delay_cmd cmd = {.id = CMD_ANTENNA_DELAY,
                                        .new_antenna_delay = new_rx_dly};
        uint8_t addr_len = dw->rx_parsed.source_len;
        bytecpy((uint8_t *)&addr, (const uint8_t *)&(dw->rx_parsed.source),
                addr_len);
        dw1000_send_command(dw, addr, addr_len, (uint8_t *)&cmd,
                            sizeof(change_antenna_delay_cmd));
        chThdSleepMilliseconds(5000);
    }
}

void fresknode_minimal_passive_range_callback(
    dw1000_driver_t *dw, ranging_result_payload_t *resultmsg)
{
    /* uint32_t range_mm = tof_to_mm((resultmsg->tof)); */
    /* uint32_t systime = chVTGetSystemTimeX(); */
    /* print_addresses(dw); */

    /* chprintf(&debug_print, "%5u\n", */
    /*         range_mm); */
    (void)dw;
    chprintf(&debug_print, "%5u\n", (uint32_t)(resultmsg->kalman_pos));
    palTogglePad(GPIOC, GPIOC_LED_BLUE);
    /*
        serial_write_range(results[0].src,
                results[0].range_mm,
                (uint32_t)dw->rssid[0]);
    */
}

uint32_t random_int(void)
{
    static uint32_t last_value = 0;
    static uint32_t new_value = 0;
    uint32_t error_bits = 0;
    error_bits = RNG_SR_SEIS | RNG_SR_CEIS;
    while (new_value == last_value)
    {
        /* Check for error flags and if data is ready. */
        if (((RNG->SR & error_bits) == 0) && ((RNG->SR & RNG_SR_DRDY) == 1))
            new_value = RNG->DR;
    }
    last_value = new_value;
    return new_value;
}

void fresknode_range_callback(dw1000_driver_t *dw,
                              ranging_result_payload_t *resultmsg)
{
    int32_t range_mm = ((int32_t)tof_to_mm((uint64_t)resultmsg->tof));
    uint32_t systime = chVTGetSystemTimeX();
    update_neighbour_seen(dw->rx_parsed.source.longaddr, systime);
    chprintf(&debug_print, "RANGE:");
    print_addresses(dw);
    /* range_mm, rssi1, rssi2, rssi3, time, los, kalman range */
    chprintf(&debug_print, "%5i,%f,%f,%f,%u,%u,%f\n", range_mm,
             resultmsg->rssi[0], dw->rx_frame[0].rssi, resultmsg->rssi[1],
             systime, resultmsg->los, resultmsg->kalman_pos);

    palTogglePad(GPIOC, GPIOC_LED_BLUE);
}
