/**
 * @file positioning.c
 * @brief  Position data
 * @author Kristoffer Ödmark
 * @version 1.0
 * @date 2016-10-20
 */
#include "positioning.h"

static position_t node_position;

void SetNodePosition(position_t *pos)
{
    node_position.x = pos->x;
    node_position.y = pos->y;
    node_position.z = pos->z;
}
void GetNodePosition(position_t *pos)
{
    pos->x = node_position.x;
    pos->y = node_position.y;
    pos->z = node_position.z;
}
