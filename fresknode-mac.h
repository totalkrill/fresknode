/**
 * @file fresknode-mac.h
 * @brief
 * @author Kristoffer Ödmark
 * @version 1.0
 * @date 2016-09-13
 */
#ifndef FRESKNODE_MAC_H
#define FRESKNODE_MAC_H

#include "dw1000.h"
#include "ieee_types.h"
#include "stdint.h"

#include "positioning.h"

enum
{
    DEVICE_ANCHOR,
    DEVICE_NODE,
    DEVICE_SINK,
    DEVICE_STATIC_NODE
};

typedef struct __attribute__((packed))
{
    uint64_t network_time;
    position_t pos;
    uint8_t device;
} pingmsg_t;

#define MAX_NEIGHBOURS 6
#define ANCHOR_BLINKTIME 10000

typedef struct
{
    ieee_addr_t addr;
    uint64_t lastseen;
    position_t pos;
} neighbour_t;

void update_neighbour_seen(uint64_t addr, uint64_t now);
void check_anchors(void);
ieee_addr_t next_anchor(void);
void mac_frame_recieved(dw1000_driver_t *self);

void start_blinkerthread(void);

#endif /* FRESKNODE-MAC_H */
