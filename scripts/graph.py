#!/usr/bin/python3
from struct import *
import numpy as np
import serial
import matplotlib.pyplot as plt

ser = serial.Serial('/dev/ttyACM0', 115200,timeout = 1)
plt.ion()
r =[]
kalman = []
vel = []
i = 0
while (1):
    line = ser.readline()

    line = str(line,'utf-8')
    if i != 0:
        try:
            line = line.split(',')
            r.append(int(line[2][1:]))
            kalman.append(float(line[6][1:9]))
            vel.append(float(line[7][1:9]))
        except:
            print("error")
        if len(r) > 100:
            print(kalman[100])
            r.pop(0)
        if len(kalman) > 100:
            kalman.pop(0)
        if len(vel) > 100:
            vel.pop(0)
        if i % 10 == 0:
            plt.clf()
            plt.ylim(-3000,3000)
            plt.plot(r,'ro', kalman, 'g',vel,'b')
            plt.draw()
            plt.pause(0.05)
    i += 1


