/**
 * @file fresknode.h
 * @brief headers for the fresknode
 * @author Kristoffer Ödmark
 * @version 0.1
 * @date 2016-06-30
 */
#ifndef FRESKNODE_H
#define FRESKNODE_H
#include "dw1000.h"
#include "dw1000_twowayranging.h"

void fresknode_minimal_passive_range_callback(
    dw1000_driver_t *dw, ranging_result_payload_t *resultmsg);
void fresknode_range_callback(dw1000_driver_t *dw,
                              ranging_result_payload_t *resultmsg);
void dw1000_range_calibration_callback(dw1000_driver_t *dw,
                                       ranging_result_payload_t *resultmsg);
uint32_t random_int(void);

#endif /* FRESKNODE_H */
